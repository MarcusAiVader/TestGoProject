package main

import "net/http"

func getRoot(writer http.ResponseWriter, request *http.Request) {
	writer.Write([]byte("Hello Whatelse"))
}


func serve() {
	http.HandleFunc("/", getRoot)
	http.ListenAndServe(":3333", nil)
}
