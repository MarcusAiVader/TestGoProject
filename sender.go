package main

import (
	"io"
	"net/http"
	"time"
)

func sendGet(){
	res, err := http.Get("http://localhost:3333")
	if err != nil {
		println("Something went wrong")
		println(err)
	}
	val, _ := io.ReadAll(res.Body)
	println(string(val))
}
func send(){

	for {
		sendGet()
		time.Sleep(time.Second * 2)
	}
}
